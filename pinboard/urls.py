from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import redirect_to

from main import views as main_views

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
import settings

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'main.views.home', name='home'),
    
    (r'^accounts/', include('registration.backends.default.urls')),
    (r'^accounts/profile/$',redirect_to, {'url': '/'}),
    
    
    
    
    url(r'^user/dashboard/$',main_views.dashboard,name='user_dashboard'),
    url(r'^user/dashboard/error/(?P<error>.*)/$',main_views.dashboard,name='user_dashboard'),
    
    url(r'^user/board/(?P<user_id>\d+)/(?P<slug>.*)/$',main_views.board,name='user_board'),
    
    url(r'^user/board/new/$',main_views.new_board,name='new_board'),
    
    
    url(r'^user/new_item/$',main_views.new_item,name='new_item'),
    
    
    
    # url(r'^ontdopen/', include('ontdopen.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),

)
