from settings import MEDIA_UPLOAD_ROOT


def handle_uploaded_file(f,pk):
    f_ext = str(f).split(".")[-1]
    
    file_name = str(pk)+"."+f_ext
    
    destination = open(MEDIA_UPLOAD_ROOT+str(pk)+"."+f_ext, 'wb+')
    for chunk in f.chunks():
        destination.write(chunk)
    destination.close()
    
    return file_name