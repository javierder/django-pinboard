from django.shortcuts import render_to_response, redirect
from django.template import RequestContext

from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponse

import models

from utils import handle_uploaded_file
def home(request):
    
    c = RequestContext(request)
    
    return render_to_response("home.html",context_instance=c)


@login_required
def dashboard(request, error=None):
    c = RequestContext(request)
    c['boards'] = models.Board.objects.filter(owner=request.user).order_by("name")
    
    
    if error == "1":
        c['add_error'] = 'A board with that name already exists.'
    return render_to_response("user/dashboard.html",context_instance=c)

def board(request,user_id,slug):
    c = RequestContext(request)
    
    c['boards'] = models.Board.objects.filter(owner=request.user).order_by("name")
    
    try:
        c['board'] = models.Board.objects.get(owner=user_id,slug=slug)
    except models.Board.DoesNotExist:
        raise Http404
    c['pins'] = models.Pin.objects.filter(owner=request.user,board=c['board']).order_by("-date_created")
    
        
    return render_to_response("user/board.html",context_instance=c)
    
    
    
@login_required
def new_board(request):
    
    if request.POST['name'] != '':
        if len(models.Board.objects.filter(owner=request.user,name=request.POST['name'])) == 0:
            new_board = models.Board(owner=request.user,name=request.POST['name'])
            new_board.save()
            return redirect('user_board',user_id=request.user.pk, slug=new_board.slug)
        else:
            
            return redirect('user_dashboard',error=1) 

@login_required
def new_item(request):
    
    new_item = models.Item()
    new_item.title = request.POST['title']
    new_item.owner = request.user
    new_item.save()
    
    if request.FILES.has_key("file"):
        file_name = handle_uploaded_file(request.FILES['file'],new_item.pk)
        new_item.file_name = file_name
        new_item.save()
    else:
        pass
    
    
    new_pin = models.Pin()
    board = models.Board.objects.get(pk=request.POST['board'])
    new_pin.item = new_item
    new_pin.owner = request.user
    new_pin.board = board
    new_pin.save()
    
    return redirect('user_board',user_id=request.user.pk, slug=board.slug) 
