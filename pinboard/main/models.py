from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify


# Create your models here.

class Board(models.Model):
    owner = models.ForeignKey(User)
    name = models.CharField(max_length=250)
    slug = models.SlugField(default="")
    date_created = models.DateTimeField(auto_now_add=True)
    
    
    def save(self):
        super(Board, self).save()
        if self.slug != slugify(self.name):
            self.slug = slugify(self.name)
            self.save()
    def __str__(self):
        return self.name

class Item(models.Model):
    title = models.CharField(max_length=250)
    owner = models.ForeignKey(User)
    website = models.URLField(blank=True)
    date_created = models.DateTimeField(auto_now_add=True)
    file_name = models.CharField(max_length=250,default="")

class Pin(models.Model):
    item = models.ForeignKey(Item)
    owner = models.ForeignKey(User)
    board = models.ForeignKey(Board)
    date_created = models.DateTimeField(auto_now_add=True)